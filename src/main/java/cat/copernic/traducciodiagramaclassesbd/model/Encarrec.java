/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.traducciodiagramaclassesbd.model;

import java.time.LocalDate;

/**
 *
 * @author EstherSanchez
 */
public class Encarrec {
    private int id;
    private LocalDate data;
    private Client client;    
    private Producte listProductes[];

    public Encarrec(Client client) {
        this.client = client;
    }

    public Encarrec(int id, LocalDate data, Client client) {
        this.id = id;
        this.data = data;
        this.client = client;
    }
}
