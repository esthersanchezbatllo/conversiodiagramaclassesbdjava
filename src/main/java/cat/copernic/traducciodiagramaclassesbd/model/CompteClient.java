/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.traducciodiagramaclassesbd.model;

/**
 *
 * @author EstherSanchez
 */
public class CompteClient {
    private int id;
    private float saldo;

    public CompteClient(int id, float saldo) {
        this.id = id;
        this.saldo = saldo;
    }
}
