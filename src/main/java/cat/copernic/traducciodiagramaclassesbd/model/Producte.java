/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.traducciodiagramaclassesbd.model;

/**
 *
 * @author EstherSanchez
 */
public class Producte {
    private int id;
    private String nom;
    private float preu;

    public Producte(int id, String nom, float preu) {
        this.id = id;
        this.nom = nom;
        this.preu = preu;
    }
    
    
}
