/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.traducciodiagramaclassesbd.model;

/**
 *
 * @author EstherSanchez
 */
public class EncarrecProducte {
    private int quantitat;
    private Encarrec encarrec;
    private Producte producte;

    public EncarrecProducte(int quantitat, Encarrec encarrec, Producte producte) {
        this.quantitat = quantitat;
        this.encarrec = encarrec;
        this.producte = producte;
    }
    
    
    
}
