/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.traducciodiagramaclassesbd.model;

/**
 *
 * @author EstherSanchez
 */
public class Cadira {
    private Pota[] potes;
    private int numPotes=0;
    public Cadira() {
        this.potes = new Pota[4];
    }
    public void afegirPota(String color, float pes){
        potes[numPotes] = new Pota(color, pes);
        numPotes++;
    }
}
