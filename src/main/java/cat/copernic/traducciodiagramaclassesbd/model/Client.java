/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.traducciodiagramaclassesbd.model;

/**
 *
 * @author EstherSanchez
 */
public class Client {
    private int id;
    private String nom;
    private String direccio;  
    private String email;
    private String telefon;
    private CompteClient cmptClient;
    
     public Client(int id, String nom, String direccio, String email, String telefon) {
        this.id = id;
        this.nom = nom;
        this.direccio = direccio;
        this.email = email;
        this.telefon = telefon;
        this.cmptClient = new CompteClient(id, 0);
    }

    public Client() {
    }
}


