/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.traducciodiagramaclassesbd.model;

/**
 *
 * @author EstherSanchez
 */
public class Pota {
    private String color;
    private float pes;

    public Pota(String color, float pes) {
        this.color = color;
        this.pes = pes;
    }
    
}
