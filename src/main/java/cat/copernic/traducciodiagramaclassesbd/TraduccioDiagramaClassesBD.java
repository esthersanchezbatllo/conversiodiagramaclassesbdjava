/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package cat.copernic.traducciodiagramaclassesbd;

import cat.copernic.traducciodiagramaclassesbd.model.Cadira;
import cat.copernic.traducciodiagramaclassesbd.model.Client;

/**
 *
 * @author EstherSanchez
 */
public class TraduccioDiagramaClassesBD {

    public static void main(String[] args) {
        exempleEncarrec();
        exempleCadira();
    }
    
    public static void exempleEncarrec() {
        Client client1 = new Client();
    }

    public static void exempleCadira() {
        Cadira cadira1 = new Cadira();
        cadira1.afegirPota("Negre", 10f);
        cadira1.afegirPota("Negre", 10f);
        cadira1.afegirPota("Negre", 10f);
        cadira1.afegirPota("Negre", 10f);              
    }
    
        

}
